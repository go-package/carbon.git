// @Package carbon
// @Description a simple, semantic and developer-friendly time package for golang
// @Page github.com/dromara/carbon
// @Developer gouguoyin
// @Blog www.gouguoyin.com
// @Email 245629560@qq.com

// Package carbon is a simple, semantic and developer-friendly time package for golang.
package carbon

import (
	"time"
)

// Carbon defines a Carbon struct.
// 定义 Carbon 结构体
type Carbon struct {
	time         time.Time
	testNow      int64 // nanosecond timestamp of test now time
	layout       string
	weekStartsAt time.Weekday
	loc          *time.Location
	lang         *Language
	Error        error
}

// NewCarbon returns a new Carbon instance.
// 初始化 Carbon 结构体
func NewCarbon(time ...time.Time) Carbon {
	c := Carbon{lang: NewLanguage()}
	c.loc, c.Error = getLocationByTimezone(defaultTimezone)
	if weekday, ok := weekdays[defaultWeekStartsAt]; ok {
		c.weekStartsAt = weekday
	}
	if len(time) > 0 {
		c.time = time[0]
		c.loc = time[0].Location()
	}
	c.layout = defaultLayout
	return c
}

// DateTime defines a DateTime struct.
// 定义 DateTime 结构体
type DateTime struct {
	Carbon
}

// NewDateTime returns a new DateTime instance.
// 初始化 DateTime 结构体
func NewDateTime(carbon Carbon) DateTime {
	return DateTime{Carbon: carbon}
}

// DateTimeMilli defines a DateTimeMilli struct.
// 定义 DateTimeMilli 结构体
type DateTimeMilli struct {
	Carbon
}

// NewDateTimeMilli returns a new DateTimeMilli instance.
// 初始化 DateTimeMilli 结构体
func NewDateTimeMilli(carbon Carbon) DateTimeMilli {
	return DateTimeMilli{Carbon: carbon}
}

// DateTimeMicro defines a DateTimeMicro struct.
// 定义 DateTimeMicro 结构体
type DateTimeMicro struct {
	Carbon
}

// NewDateTimeMicro returns a new DateTimeMicro instance.
// 初始化 DateTimeMicro 结构体
func NewDateTimeMicro(carbon Carbon) DateTimeMicro {
	return DateTimeMicro{Carbon: carbon}
}

// DateTimeNano defines a DateTimeNano struct.
// 定义 DateTimeNano 结构体
type DateTimeNano struct {
	Carbon
}

// NewDateTimeNano returns a new DateTimeNano instance.
// 初始化 DateTimeNano 结构体
func NewDateTimeNano(carbon Carbon) DateTimeNano {
	return DateTimeNano{Carbon: carbon}
}

// Date defines a Date struct.
// 定义 Date 结构体
type Date struct {
	Carbon
}

// NewDate returns a new Date instance.
// 初始化 Date 结构体
func NewDate(carbon Carbon) Date {
	return Date{Carbon: carbon}
}

// DateMilli defines a DateMilli struct.
// 定义 DateMilli 结构体
type DateMilli struct {
	Carbon
}

// NewDateMilli returns a new DateMilli instance.
// 初始化 DateMilli 结构体
func NewDateMilli(carbon Carbon) DateMilli {
	return DateMilli{Carbon: carbon}
}

// DateMicro defines a DateMicro struct.
// 定义 DateMicro 结构体
type DateMicro struct {
	Carbon
}

// NewDateMicro returns a new DateMicro instance.
// 初始化 DateMicro 结构体
func NewDateMicro(carbon Carbon) DateMicro {
	return DateMicro{Carbon: carbon}
}

// DateNano defines a DateNano struct.
// 定义 DateNano 结构体
type DateNano struct {
	Carbon
}

// NewDateNano returns a new DateNano instance.
// 初始化 DateNano 结构体
func NewDateNano(carbon Carbon) DateNano {
	return DateNano{Carbon: carbon}
}

// Time defines a Time struct.
// 定义 Time 结构体
type Time struct {
	Carbon
}

// NewTime returns a new Time instance.
// 初始化 Time 结构体
func NewTime(carbon Carbon) Time {
	return Time{Carbon: carbon}
}

// TimeMilli defines a TimeMilli struct.
// 定义 TimeMilli 结构体
type TimeMilli struct {
	Carbon
}

// NewTimeMilli returns a new TimeMilli instance.
// 初始化 TimeMilli 结构体
func NewTimeMilli(carbon Carbon) TimeMilli {
	return TimeMilli{Carbon: carbon}
}

// TimeMicro defines a TimeMicro struct.
// 定义 TimeMicro 结构体
type TimeMicro struct {
	Carbon
}

// NewTimeMicro returns a new TimeMicro instance.
// 初始化 TimeMicro 结构体
func NewTimeMicro(carbon Carbon) TimeMicro {
	return TimeMicro{Carbon: carbon}
}

// TimeNano defines a TimeNano struct.
// 定义 TimeNano 结构体
type TimeNano struct {
	Carbon
}

// NewTimeNano returns a new TimeNano instance.
// 初始化 TimeNano 结构体
func NewTimeNano(carbon Carbon) TimeNano {
	return TimeNano{Carbon: carbon}
}

// Timestamp defines a Timestamp struct.
// 定义 Timestamp 结构体
type Timestamp struct {
	Carbon
}

// NewTimestamp returns a new Timestamp instance.
// 初始化 Timestamp 结构体
func NewTimestamp(carbon Carbon) Timestamp {
	return Timestamp{Carbon: carbon}
}

// TimestampMilli defines a TimestampMilli struct.
// 定义 TimestampMilli 结构体
type TimestampMilli struct {
	Carbon
}

// NewTimestampMilli returns a new TimestampMilli instance.
// 初始化 TimestampMilli 结构体
func NewTimestampMilli(carbon Carbon) TimestampMilli {
	return TimestampMilli{Carbon: carbon}
}

// TimestampMicro defines a TimestampMicro struct.
// 定义 TimestampMicro 结构体
type TimestampMicro struct {
	Carbon
}

// NewTimestampMicro returns a new TimestampMicro instance.
// 初始化 TimestampMicro 结构体
func NewTimestampMicro(carbon Carbon) TimestampMicro {
	return TimestampMicro{Carbon: carbon}
}

// TimestampNano defines a TimestampNano struct.
// 定义 TimestampNano 结构体
type TimestampNano struct {
	Carbon
}

// NewTimestampNano returns a new TimestampNano instance.
// 初始化 TimestampNano 结构体
func NewTimestampNano(carbon Carbon) TimestampNano {
	return TimestampNano{Carbon: carbon}
}
